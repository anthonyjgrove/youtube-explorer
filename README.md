YouTube Explorer built with React and Node


### NPM Scripts

###### Build on Dev:
```
npm run build:dev
```
###### Build on Prod:
```
npm run build
```
###### Run Node on Prod:
```
npm run dev
```
###### Run Node on Prod:
```
npm run start
```

Follow me on Twitter: [@anthonyjgrove](https://twitter.com/anthonyjgrove)